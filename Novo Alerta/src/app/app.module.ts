import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera, CameraOptions } from '@ionic-native/camera';


import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegistroPage } from '../pages/registro/registro';
import { EnvioPage } from '../pages/envio/envio';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Geolocation } from '@ionic-native/geolocation';

const config = {
    apiKey: "AIzaSyAZYhB6Q226F1pwjAYCh1_v-1-3yPD4KKc",
    authDomain: "rp-alerta-fatec.firebaseapp.com",
    databaseURL: "https://rp-alerta-fatec.firebaseio.com",
    projectId: "rp-alerta-fatec",
    storageBucket: "rp-alerta-fatec.appspot.com",
    messagingSenderId: "914823199868"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegistroPage,
    EnvioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireAuthModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegistroPage,
    EnvioPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler}, Geolocation,SplashScreen,Camera
    
  ]
})
export class AppModule {}
