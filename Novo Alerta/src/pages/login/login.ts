import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth'
import { AlertController, NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegistroPage } from '../registro/registro';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';


@Component({
  selector: 'app-login',
  templateUrl: './login.html'
})
export class LoginPage {
    
  constructor(private afAuth : AngularFireAuth, 
              private alertControl : AlertController,
              private navCtrl : NavController
              ) { 
                
              }

  loginGoogle(){
    let provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().useDeviceLanguage();
    firebase.auth().signInWithPopup(provider).then(function() {

      this.navCtrl.setRoot(HomePage);

    }).catch(error => {
        let msg = "";
        switch (error.code) { 
          case "auth/wrong-password":
            msg = "Senha inválida para o e-mail digitado.";
            break;

          case "auth/user-not-found":
            msg = 'Usuário não encontrado'
            break;

          case "auth/invalid-email":
            msg = 'E-mail inválido.';
            break;
          default:
            msg = "Ocorreu um erro ao tentar logar com o Google.";
        }

        if(msg != ""){
          let alert = this.alertControl.create({
            title: 'Falha no acesso',
            subTitle: msg,
            buttons: ["Ok"]
          });
          alert.present();
        }
        
    });
      
  }

  loginFacebook(){
    var provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().useDeviceLanguage();
    firebase.auth().signInWithPopup(provider).then(function() {

      this.navCtrl.setRoot(HomePage);

    }).catch(error => {
        let msg = "";
        switch (error.code) { 
          case "auth/wrong-password":
            msg = "Senha inválida para o e-mail digitado.";
            break;

          case "auth/user-not-found":
            msg = 'Usuário não encontrado'
            break;

          case "auth/invalid-email":
            msg = 'E-mail inválido.';
            break;
          default:
            msg = "Ocorreu um erro ao tentar logar com o Facebook.";
        }

        if(msg != ""){
          let alert = this.alertControl.create({
            title: 'Falha no acesso',
            subTitle: msg,
            buttons: ["Ok"]
          });
          alert.present();
        }
        
    });
      
  }

  entrar(form: NgForm) {
    let email : string = form.value.email;
    let senha : string = form.value.senha;

    this.afAuth.auth.signInWithEmailAndPassword(email, senha)
    
      .then(user => {
        this.navCtrl.setRoot(HomePage);
      })
      .catch(error => {
        let msg;
        switch (error.code) { 
          case "auth/wrong-password":
            msg = "Senha inválida para o e-mail digitado.";
            break;

          case "auth/user-not-found":
            msg = 'Usuário não encontrado'
            break;

          case "auth/invalid-email":
            msg = 'E-mail inválido.';
            break;
          default:
            msg = "Ocorreu um erro ao tentar logar.";
        }

        let alerta = {
          title: 'Falha no acesso',
          subTitle: msg,
          buttons: ["Ok"]
        }
        this.alertControl.create(alerta).present();
      })
  }

  public registrar(): void {
    this.navCtrl.push(RegistroPage);
  }
}