import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { Denuncia } from '../../models/denuncia';
import { AngularFirestore } from 'angularfire2/firestore';
import { NavParams, LoadingController } from 'ionic-angular';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-envio',
  templateUrl: './envio.html'
})
export class EnvioPage {

  public denuncia: Denuncia;

  foto:any;
  constructor(public loadingCtrl: LoadingController,
              public navCtrl : NavController, 
              public camera: Camera, 
              public afAuth: AngularFireAuth, 
              private geolocation: Geolocation, 
              public params: NavParams, 
              public db: AngularFirestore) { 
    let uid = params.get('uidUsuario');
      if(uid != undefined){
        this.denuncia = uid;
      }else{
        this.iniciarCamera();
      }
   }
   public user = {};
  salvar(form: NgForm){
    let loading = this.loadingCtrl.create(
      {
        content: 'Por favor, aguarde...'
      });
    loading.present();
    this.db.collection('usuarios').doc(this.afAuth.auth.currentUser.uid).valueChanges().subscribe((user)=> {
      this.user= user;
    })
   //pega localização atual e joga em variável para enviar para o banco
    this.geolocation.getCurrentPosition().then((resp) => {
    let dia = new Date();
     //this.lista = this.db.collection('usuarios').doc(this.afAuth.auth.currentUser.uid).ref.get();
        let denu ={
          descricao: form.value.descricao,
          categoria: form.value.categoria, 
          uidUsuario: this.afAuth.auth.currentUser.uid,
          usuario: this.user,
          localizacao:  {
            latitude: resp.coords.latitude,
            longitude: resp.coords.longitude
          },
          data: dia
      };
      this.db.collection('denuncias').add(denu).then((d)=>{
        let pasta = this.afAuth.auth.currentUser.uid;
        var storage = firebase.storage(); 
        var storageRef = storage.ref().child(pasta+"/"+ d.id +'.jpg');
    
        storageRef.putString(this.foto,'data_url').then((x)=>{
          //console.log(x.downloadURL);
        this.db.collection('denuncias').doc(d.id).update({
          
          
          imagem : x.downloadURL
        })});
        //console.log(storage);
        loading.dismiss();
        this.navCtrl.pop();
      });
          
      //storag.putString(this.foto, 'base64').then(function(snap){
       // var uploadTask = storageRef.child('images/mountains.jpg').put(this.foto);
      
      //console.log(this.foto);
    //console.log(resp.coords);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    
  }

  iniciarCamera(){
      const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 640,
      targetHeight: 320
    }
    
      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64
        this.foto = 'data:image/jpeg;base64,' + imageData;
       }, (err) => {
        // Handle error
       });
     
    }

  voltar(){
    this.navCtrl.pop();
  }
  public logout(): void{
    this.afAuth.auth.signOut();
  }
}