import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth'
import { AlertController, NavController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.html'
})
export class RegistroPage {

  constructor(private afAuth : AngularFireAuth, 
              private alertControl : AlertController,
              private navCtrl : NavController,
              private db:AngularFirestore) { }

  registrar(form: NgForm) {
    let email: string = form.value.email;
    let senha: string = form.value.senha;
    let nome: string = form.value.nome;

    this.afAuth.auth.createUserWithEmailAndPassword(email, senha)
      .then((user) =>{
        this.db.collection("usuarios").doc(user.uid)
          .set({
            nome: nome,
            email: email
          })
      })
      .catch((error) => {
        let erro = "";
        switch(error.code){
          case "auth/email-already-in-use":
            erro = "O email já está sendo usado.";
          break;
          case "auth/invalid-email":
            erro = "Email inválido.";
          break;
          case "auth/weak-password":
            erro = "Senha muito fraca.";
          break;
          default:
            erro = "Ocorreu um erro ao cadastrar.";
        }

        this.alertControl.create({
          title: "Erro no registro",
          subTitle: erro,
          buttons:["Ok"]
        }).present();
      })

  }

  public voltar(){
    this.navCtrl.pop();
  }
}