import { Component } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore'
import { Observable } from 'rxjs/Observable';
import { Denuncia } from '../../models/denuncia';
import { AngularFireAuth } from 'angularfire2/auth';
import { NavController } from 'ionic-angular';
import {EnvioPage } from '../envio/envio';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public lista: Observable<Denuncia[]>;

    constructor(public db: AngularFirestore, public afAuth: AngularFireAuth, public navCtrl: NavController) {
    let uid = afAuth.auth.currentUser.uid;
    
    this.lista = db.collection<Denuncia>('denuncias', ref => ref.where('uidUsuario','==', uid).orderBy('data')).valueChanges().map(items => items.sort().reverse());

  }
  
    public tiraFoto(){
      this.navCtrl.push(EnvioPage);
  }
  
    public detalhes(uid): void{
      this.navCtrl.push(EnvioPage, {uidUsuario: uid});
    }
 
  public logout(): void{
    this.afAuth.auth.signOut();
  }
}
  